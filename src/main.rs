extern crate cgi;

fn main() { cgi::handle(|_request: cgi::Request| -> cgi::Response {
    cgi::html_response(200, "<html><body><h1>Hello World!</h1></body></html>\n")
})}