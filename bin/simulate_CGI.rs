fn main() {
use std::env;
// use std::path::Path;
use std::process::Command;
// use std::{thread, time};

// println!("{}", Path::new("target/debug/hello").exists());

env::set_var("RUST_BACKTRACE", "1");
env::set_var("SERVER_SOFTWARE", "Rust");
env::set_var("SERVER_NAME", "server_name");
env::set_var("GATEWAY_INTERFACE", "CGI/1.1");
env::set_var("SERVER_PROTOCOL", "HTTP/1.1");
env::set_var("SERVER_PORT", "80");
env::set_var("REQUEST_METHOD", "GET");
env::set_var("PATH_INFO", "/");
env::set_var("PATH_TRANSLATED", "/");
env::set_var("SCRIPT_NAME", "hello");
env::set_var("REMOTE_ADDR", "8.8.8.8");
env::set_var("CONTENT_TYPE", "text/plain");
env::set_var("QUERY_STRING", "");
env::set_var("REMOTE_HOST", "");
env::set_var("CONTENT_LENGTH", "");
env::set_var("HTTP_USER_AGENT", "");
env::set_var("HTTP_COOKIE", "");
env::set_var("HTTP_REFERER", "");
Command::new("time")
        .arg("target/debug/hello")
        .spawn()
        .expect("target/debug/hello command failed to start");


// match env::var("OUTPUT_SYNCHRO") {
    // Ok(val) => 
        // match val.trim().parse::<u64>() { // .expect("OUTPUT_SYNCHRO: x.xxx [seconds]") {
            // duration =>
                // thread::sleep(time::Duration::from_millis(duration*1000)),
            // _ => (),
        // }
    // Err(_) => (),
// }
}
