# cgi

[**cgi**](https://crates.io/crates/cgi)
![Crates.io (recent)](https://img.shields.io/crates/dr/cgi)
Common Gateway Interface based on [**http**](https://crates.io/crates/http) types
